﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPages2
{
    public class MainController : Controller
    {
        public IActionResult Weather()
        {
            string r = App_Start.Utilities.rest(@"http://api.openweathermap.org/data/2.5/weather?zip=27407,us&units=imperial&APPID=be773ff1a5983ce003b8a907caedf3aa");
            return Json(r);
        }


    }
}
